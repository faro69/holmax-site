($(document).on("ready", function() {
    var up = $(".to_up");

    $("div[data-src],a[data-src]").each(function() {
        setElementBackground.apply(this);
    });

    function setElementBackground() {
        var elem = $(this);
        var src = elem.data("src");
        elem.css("background-image", `url(${src})`);
    }
  
}))
